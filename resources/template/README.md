# _NOME__

_Descrizione_

## ✔️ Soluzioni privacy-friendly

- [Soluzione 1](#soluzione1)

### Soluzione 1

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ✔️ SI   |
| I dati personali rimangono su server EU e sotto GDPR      | ✔️ SI   |
| Legislazione dei dati                                     | ✔️ 🇪🇺🇩🇪 |

**DATA PROCESSOR:**

_descrizione e link a data processor_


[Esempio completo »](snippets/nome)

---

## ❌ Da evitare

- [Soluzione 2](#soluzione2)
- [Soluzione 3](#soluzione3)

### Soluzione 2

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |

---

### Soluzione 3

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |