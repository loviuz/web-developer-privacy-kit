# Font locali

E' sufficiente scaricare i font da siti web dove è possibile scaricarli liberamente, come ad esempio [DaFont](https://www.dafont.com/it/), scaricare il font e includerlo via CSS:

**ATTENZIONE:** ogni font ha una licenza d'uso, leggila prima di utilizzare un font nel tuo progetto.

Prova con [Tango Sans](https://www.dafont.com/it/tangosans.font):

```css
@font-face {
    font-family: TangoSans;
    src: url(../fonts/TangoSans.ttf);
}

body {
    font-family: TangoSans;
}
```