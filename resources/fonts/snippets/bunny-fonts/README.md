# Bunny Fonts

E' sufficiente includere i font da `fonts.bunny.net` e impostarli nel CSS:

**ATTENZIONE:** ogni font ha una licenza d'uso, leggila prima di utilizzare un font nel tuo progetto.

Prova con [Aclonica](https://fonts.bunny.net/family/aclonica):

```html
<style>
@import url(https://fonts.bunny.net/css?family=aclonica:400);
</style>
```

```css
body {
    font-family: Aclonica;
}
```