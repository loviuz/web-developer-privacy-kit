# Fonts

Metodo di inclusione font su pagine web.

## ✔️ Soluzioni privacy-friendly

- [Inclusione font locali](#inclusione-font-locali)
- [Bunny Fonts](#bunny-fonts)

## ❌ Da evitare

- [Google Fonts](#google-fonts)

### Inclusione font locali

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | -      |
| Utilizzabile interamente sul proprio server               | ✔️ SI   |

[Esempio completo »](snippets/font-locali)

Se hai bisogno di scaricare i font provenienti da Google Fonts per includerli localmente nel tuo sito puoi usare [Google Webfonts Helper](../../#strumenti)


### Bunny Fonts

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | -      |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ✔️ SI   |
| I dati personali rimangono su server EU e sotto GDPR      | ✔️ SI   |
| Ubicazione server                                         | ✔️ 🇪🇺🇮🇹 |
| Legislazione dei dati                                     | ✔️ 🇪🇺🇸🇮 |

**DATA PROCESSOR:**

[BunnyWay d.o.o.](https://bunny.net/tos/), Cesta komandanta Staneta 4A, 1215 Medvode, Slovenia

[Esempio completo »](snippets/bunny-fonts)


---

### Google Fonts

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Ubicazione dei dati                                       | ❌ 🇺🇸  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |

**DATA PROCESSOR:**

[Google Ireland Limited](https://policies.google.com/privacy#europeanrequirements), Gordon House, Barrow Street, Dublin 4, Irlanda

controllata da:

[Google LLC](https://developers.google.com/terms), 1600 Amphitheatre Parkway, Mountain View, California 94043, Stati Uniti
