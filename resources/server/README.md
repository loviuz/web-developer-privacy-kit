# Server

Servizi di hosting rispettosi della privacy con responsabile trattamento dati in UE e sottoposto al controllo del GDPR.

## ✔️ Soluzioni privacy-friendly

- Servizio di hosting in UE con azienda con sede in UE

Per un elevato livello di riservatezza puoi provare un servizio di hosting più sicuro:

https://www.privacytools.io/private-hosting/

---

## ❌ Da evitare

- [Amazon Hosting - AWS](#amazon-hosting-aws)
### Amazon Hosting - AWS

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | -      |
| Utilizzabile interamente sul proprio server               | ❌ NO  |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |

**DATA PROCESSOR:**

[Amazon Web Services, Inc.](https://aws.amazon.com/it/compliance/sub-processors/), Stati Uniti
