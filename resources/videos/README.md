# Video

Metodi di inclusione di video o alternative di streaming video rispettose della privacy.

## ✔️ Soluzioni privacy-friendly

- [Peertube](#peertube)
- [Bunny Video](#bunny-video)

## ❌ Da evitare

- [YouTube](#youtube)
- [Vimeo](#vimeo)

### Peertube

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ✔️ SI   |
| In cloud su server europeo sotto GDPR                     | ✔️ SI   |
| I dati personali rimangono su server EU e sotto GDPR      | ✔️ SI   |
| Ubicazione server                                         | dipende dal server |
| Legislazione dei dati                                     | dipende dal server |

Una lista di istanze Peertube è disponibile qui: https://instances.joinpeertube.org/instances

E' possibile ospitare un'istanza sul proprio server.

Dal video stesso c'è la possibilità di ottenere il codice per includere il video nella propria pagina web.

---

### Bunny Video

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO   |
| Utilizzabile interamente sul proprio server               | ❌ NO   |
| In cloud su server europeo sotto GDPR                     | ✔️ SI   |
| I dati personali rimangono su server EU e sotto GDPR      | ✔️ SI   |
| Ubicazione server                                         | ✔️ 🇪🇺🇸🇮 |
| Legislazione dei dati                                     | ✔️ 🇪🇺🇸🇮 |

[BunnyWay d.o.o.](https://bunny.net/tos/), Cesta komandanta Staneta 4A, 1215 Medvode, Slovenia

Esempio completo e personalizzabile sul sito web:

https://bunny.net/stream/

---

### YouTube

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ❌ NO  |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |

YouTube ha messo a disposizione l'inclusione di video senza cookie con l'uso del dominio youtube-nocookie.com e infatti tramite questo URL non genera cookie, ma genera un id non documentato (`yt-remote-device-id`) che viene installato nello storage locale del browser:

https://www.cookiebot.com/media/1136/cookiebot-report-2019-ad-tech-surveillance-2.pdf

**DATA PROCESSOR:**

[Google Ireland Limited](https://policies.google.com/privacy#europeanrequirements), Gordon House, Barrow Street, Dublin 4, Irlanda

controllata da:

[Google LLC](https://developers.google.com/terms), 1600 Amphitheatre Parkway, Mountain View, California 94043, Stati Uniti


---

### Vimeo

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ❌ NO  |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |

**DATA PROCESSOR:**

[Vimeo.com Inc](https://vimeo.com/privacy#how_to_contact_us), 330 West 34th Street, 5th Floor, New York, New York 10001
