# Openstreetmap con tile server europeo

Viene utilizzato LeafletJS + una libreria per migliorare l'esperienza utente. E' importante specificare l'utilizzo di un tile server europeo anche se non è l'unico. Altri tile server utilizzabili è possibile trovali qui: https://wiki.openstreetmap.org/wiki/Tile_servers

## Risorse utilizzate

### LeafletJS

Libreria javascript open source per mappe mobil-friendly e interattive
- [Sito web](https://leafletjs.com/)
- [Collabora](https://github.com/Leaflet/Leaflet)

**Leaflet Gesture Handling**

Libreria javascript per migliorare l'esperienza utente con LeafletJS, evitando che lo scroll della rotellina del mouse usata per scorrere la pagina attivi lo zoom della mappa, forzando così l'uso della combinazione CTRL+rotellina mouse.
- [Collabora](https://github.com/elmarquis/Leaflet.GestureHandling)
