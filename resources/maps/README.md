# Mappe

Metodo di inclusione di mappe su pagine web.

## ✔️ Soluzioni privacy-friendly

- [Openstreetmap con tile server europeo](#openstreetmap-con-tile-server-europeo)

## ❌ Da evitare

- [Google Maps](#google-maps)
- [Openstreetmap su server USA](#openstreetmap-su-server-usa)

### Openstreetmap con tile server europeo

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ✔️ SI   |
| I dati personali rimangono su server EU e sotto GDPR      | ✔️ SI   |
| Legislazione dei dati                                     | ✔️ 🇪🇺🇩🇪 |

[Esempio completo »](snippets/osm-eu)

---

### Google Maps

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |

**DATA PROCESSOR:**

[Google Ireland Limited](https://policies.google.com/privacy#europeanrequirements), Gordon House, Barrow Street, Dublin 4, Irlanda

controllata da:

[Google LLC](https://developers.google.com/terms), 1600 Amphitheatre Parkway, Mountain View, California 94043, Stati Uniti

---

### Openstreetmap su server USA

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Legislazione dei dati                                     | ❌ 🇬🇧  |

**DATA PROCESSOR:**

[OpenStreetmap Foundation](https://wiki.osmfoundation.org/wiki/Contact), St John’s Innovation Centre, Cowley Road, Cambridge, CB4 0WS, United Kingdom 
