# Calendario

Strumenti per l'inclusione di un calendario web.


## ✔️ Soluzioni privacy-friendly

- [FullCalendar locale](#fullcalendar-locale)

## ❌ Da evitare

- [Google Calendar](#google-calendar)
### FullCalendar locale

| Checklist                                                 | Esito   |
| ----------------------------------------------------------| :------ |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ✔️ SI   |

[Esempio completo »](snippets/fullcalendar)

---

### Google Calendar

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ❌ NO   |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |

**DATA PROCESSOR:**

[Google Ireland Limited](https://policies.google.com/privacy#europeanrequirements), Gordon House, Barrow Street, Dublin 4, Irlanda

controllata da:

[Google LLC](https://developers.google.com/terms), 1600 Amphitheatre Parkway, Mountain View, California 94043, Stati Uniti
