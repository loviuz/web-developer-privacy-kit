# FullCalendar

[FullCalendar](https://fullcalendar.io/) é un componente javascript che permette di visualizzare in forma interattiva un calendario su pagina web, permettendo di visualizzare eventi ma anche di aggiungerne e modificarli. L'esempio di seguito serve per creare un calendario che visualizza eventi manualmente, provenienti per esempio da un database.

Può essere usato anche per includere un calendario condiviso tramite il protocollo Dav e il format iCal:

https://fullcalendar.io/docs/icalendar


PS: lo snippet è stato preso da qui:

https://github.com/fullcalendar/fullcalendar-examples

