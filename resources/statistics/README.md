# Statistiche

Strumenti di statistiche per il conteggio e l'analisi dei visitatori del proprio sito web.

## ✔️ Soluzioni privacy-friendly

- [Matomo, self-hosted](#matomo-self-hosted)
- [Matomo cloud](#matomo-cloud)
- [GoatCounter, self-hosted](#goatcounter-self-hosted)
- [GoatCounter cloud](#goatcounter-cloud)
- [Open Web Analytics, self-hosted](#open-web-analytics-self-hosted)

## ❌ Da evitare

- [Google Analytics](#google-analytics)

### Matomo, self-hosted

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ✔️ SI   |

[Esempio completo »](https://matomo.org/matomo-on-premise/)

---

### Matomo cloud

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ✔️ SI   |
| I dati personali rimangono su server EU e sotto GDPR      | ✔️ SI   |
| Ubicazione server                                         | ✔️ 🇪🇺🇳🇿 |
| Legislazione dei dati                                     | ✔️ 🇪🇺🇩🇪 |

**DATA PROCESSOR:**

[InnoCraft Ltd](https://matomo.org/matomo-cloud-privacy-policy/), 7 Waterloo Quay PO625, 6140 Wellington, New Zealand

[Esempio completo »](https://matomo.org/pricing/)

---

### GoatCounter, self-hosted

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ✔️ SI   |

[Esempio completo »](https://github.com/arp242/goatcounter)

---

### GoatCounter cloud

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ✔️ SI   |
| I dati personali rimangono su server EU e sotto GDPR      | ✔️ SI   |
| Ubicazione server                                         | ✔️ 🇪🇺🇩🇪 |
| Legislazione dei dati                                     | ✔️ 🇪🇺🇳🇱 |

**DATA PROCESSOR:**

[Martin Tournoij](https://www.arp242.net/cv/cv-martintournoij), Eindhoven, Netherlands

[Esempio completo »](https://www.goatcounter.com/)

---

### Open Web Analytics, self-hosted

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ✔️ SI   |
| Utilizzabile interamente sul proprio server               | ✔️ SI   |

[Esempio completo »](https://www.openwebanalytics.com/)

---

### Google Analytics

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Ubicazione server                                         | ❌ 🇺🇸  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |

**DATA PROCESSOR:**

[Google Ireland Limited](https://policies.google.com/privacy#europeanrequirements), Gordon House, Barrow Street, Dublin 4, Irlanda

controllata da:

[Google LLC](https://developers.google.com/terms), 1600 Amphitheatre Parkway, Mountain View, California 94043, Stati Uniti
