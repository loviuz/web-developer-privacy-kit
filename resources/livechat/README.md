# Livechat

Servizi di Livechat su pagine web.

## ✔️ Soluzioni privacy-friendly

- [Userlike](https://www.userlike.com/)
- [Crisp](https://crisp.chat/)

## ❌ Da evitare

- [Tawk](#tawk)
- [Trengo](https://trengo.com/)

### Userlike

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ✔️ SI   |
| I dati personali rimangono su server EU e sotto GDPR      | ✔️ SI   |
| Legislazione dei dati                                     | ✔️ 🇪🇺🇩🇪 |

**DATA PROCESSOR:**

[Userlike UG (limited liability)](https://www.userlike.com/it/terms), Probsteigasse 44-46, 50670 Cologne, Germany

---

### Crisp

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ✔️ SI   |
| I dati personali rimangono su server EU e sotto GDPR      | ✔️ SI   |
| Legislazione dei dati                                     | ✔️ 🇪🇺🇩🇪 |

**DATA PROCESSOR:**

[Crisp IM](https://crisp.chat/en/terms/), 2 boulevard de Launay, Nantes, France

---


### Tawk

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |

**DATA PROCESSOR:**

[tawk.to Inc.](https://www.tawk.to/data-protection/dpa-data-processing-addendum/), 187 East Warm Springs Rd, SB298, Las Vegas, Nevada, 89119

---

### Trengo

| Checklist                                                 | Esito  |
| ----------------------------------------------------------| :----- |
| Software libero                                           | ❌ NO  |
| Utilizzabile interamente sul proprio server               | ⚠️ NO   |
| In cloud su server europeo sotto GDPR                     | ❌ NO  |
| I dati personali rimangono su server EU e sotto GDPR      | ❌ NO  |
| Legislazione dei dati                                     | ❌ 🇺🇸  |

**DATA PROCESSOR:**

[Trengo B.V](https://trengo.com/en/privacy-statement/), Burgemeester Reigerstraat 89, 3581 KP Utrecht, Chamber of Commerce: 72043687
