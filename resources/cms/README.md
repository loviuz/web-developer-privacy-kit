# CMS

Guide e info utili per rendere i CMS e le loro estensioni GDPR-compliant.

![Joomla logo](images/Joomla_logo.png)
- [Template per le scuole](https://www.joomla.it/blog/9026-il-nuovo-template-joomla-per-le-scuole.html)
---

![Drupal logo](images/Drupal_logo.png)
- [Rimozione Google Fonts da tema PASW](https://www.paswdrupal.net/articolo/pec-da-monitora-pait)
- [Rimozione Google Fonts genericamente](http://monitora-pa.it/2022/08/17/guida_google-fonts_local-DRUPAL.pdf)

---


![Wordpress logo](images/WordPress_logo.png)
- [Plugin per la rimozione di Google Fonts](https://wordpress.org/plugins/disable-remove-google-fonts/)
- [Come trovare e bloccare componenti che usano risorse non GDPR](https://monitora-pa.it/2022/12/07/MonitoraPA_Guida_WordPress.pdf)
