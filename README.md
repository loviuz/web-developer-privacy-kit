# Web Developer Privacy Kit

Un archivio completo di guide, snippet di codice, servizi e suggerimenti per gli sviluppatori di siti e applicazioni web orientati alla privacy.

![Header](images/header.png)

## ❓ Perchè

Questo repository è orientato agli sviluppatori web che vogliono rendere il web un posto sicuro. Lo scopo è fornire risorse per creare siti e applicazioni web orientati alla privacy-by-design, e soluzioni in alternativa alle principali aziende tecnologiche, così da stimolare la concorrenza e ridurre i monopoli tecnologici.

## 🚀 Come iniziare

| 🗄️ Tipologia                               | ✔️ Risorse sicure e GDPR-compliant | ❌ Risorse non sicure |
| ------------------------------------------ | -----------------: | --------------------: |
| [Calendario](resources/calendar/)          |                  1 |                     1 |
| [CMS](resources/cms/)                      |                  1 |                     0 |
| [Form](resources/forms/)                   |                  0 |                     0 |
| [Fonts](resources/fonts/)                  |                  2 |                     1 |
| [LiveChat](resources/livechat/)            |                  2 |                     2 |
| [Mappe](resources/maps/)                   |                  1 |                     2 |
| [Pubblicità](resources/ads/)               |                  0 |                     0 |
| [Server](resources/server/)                |                  1 |                     1 |
| [Statistiche](resources/statistics/)       |                  5 |                     1 |
| [Video](resources/videos/)                 |                  1 |                     1 |


E' possibile usare la cartella [template](resources/template) per redigere i contenuti di ogni tipologia o creare nuove tipologie.

## 🧰 Strumenti

| Nome                   | Descrizione                      |
| ---------------------- | -------------------------------- |
| [Webbkoll](https://webbkoll.dataskydd.net/it/)               | Verifica script, cookie e altre tecnologie presenti in un sito web per valutare l'impatto sulla privacy |
| [Google Fonts Checker](https://fontsplugin.com/google-fonts-checker/)   | Verifica se un sito web usa i Google Fonts |
| [MonitoraPA](https://github.com/MonitoraPA/monitorapa) | Software di monitoraggio della conformità GDPR di siti e applicazioni web |
| [Google Webfonts Helper](https://gwfh.mranftl.com/fonts/) | Strumento per scaricare i font da Google Fonts per includerli localmente nel proprio sito web |
| [LSTU](https://lstu.fr/) | Servizio basato su software libero e GDPR-compliant per abbreviare lunghi URL |
| [uBlock Origin](https://ublockorigin.com/) | Estensione per browser che consente di visualizzare tutte le risorse esterne utilizzate scaricate dal sito web sul browser dell'utente |




## ⚙️ Metodo di lavoro

Non ci sono al momento regole specifiche per il metodo di lavoro.

Per discutere di soluzioni o alternative occorre creare un'issue tramite **Problemi** e attenderne la discussione. Quando la soluzione è convincente verrà integrata nel repository oppure è preferibile effettuare una pull request, oppure se si è già contributori si può eseguire direttamente il commit.

La categorizzazione delle alternative viene gestita in sotto-cartelle come ad esempio `server`, `statistics`, `fonts`, `maps`, `videos`, `calendar`, ecc.

Soluzioni speciali come ad esempio modifiche su un tema di Wordpress per renderlo privacy-friendly potranno essere gestite in una sotto-cartella con il nome `wordpress`.


## 🧡 Risorse esterne

I seguenti progetti sono utili per il rilevamento di risorse da evitare o propongono diverse soluzioni implementabili o anche solo accennate:
 - [Monitora PA](https://github.com/MonitoraPA/monitorapa)
 - [Privacy Guides](https://www.privacyguides.org/tools/)
 - [PrivacyTools](https://www.privacytools.io/)
 - [Le Alternative](https://www.lealternative.net/)
 - [European Alternatives](https://european-alternatives.eu/)

Per analizzare le tecnologie usate lato client è possibile appoggiarsi a [Webbkoll](https://webbkoll.dataskydd.net/). Lato server non è possibile eseguire verifiche automatizzate.

## 👐 Licenza

Web Developer Privacy Kit © 2022 by Fabio Lovato is licensed under CC BY 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/

L'immagine in intestazione è scaricata da Pixabay e rielaborata: https://pixabay.com/it/vectors/il-computer-portatile-tavoletta-2017978/

## ☑️ TODO
- [X] definire una struttura delle cartelle
- [X] definire degli strumenti esterni per la verifica della presenza o meno di risorse non privacy-friendly in siti e applicazioni web
- [ ] coinvolgere altri contributori
- [ ] Tradurre i vari file markdown almeno in inglese per ampliare il pubblico